package browser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;
public class Windows1 
{
public static void main(String[] args) 
{
	System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://www.facebook.com/");
	driver.manage().window().maximize();
	driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
	WebElement we = driver.findElement(By.xpath(".//*[@id='privacy-link']"));
	we.click();
	ArrayList<String> wintabs = new ArrayList<String>(driver.getWindowHandles());
	System.out.println(wintabs.size());
	String parentid = wintabs.get(0);
	String childid = wintabs.get(1);
	System.out.println("parent page title is :"+driver.getTitle());
	driver.switchTo().window(childid);
	System.out.println("child page title is :"+driver.getTitle());
	driver.switchTo().window(parentid);
	driver.findElement(By.xpath(".//input[@id='email']")).sendKeys("Prasanth");
	System.out.println("parent page title is :"+driver.getTitle());
}
}
