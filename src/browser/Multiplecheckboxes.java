package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import java.util.List;
public class Multiplecheckboxes 
{	
public static void main(String[] args) 
{
WebDriver driver = new FirefoxDriver();
driver.manage().window().maximize();
driver.get("http://www.seleniumeasy.com/test/basic-checkbox-demo.html");
List <WebElement> checks=driver.findElements(By.className("cb1-element"));
System.out.println(checks.size());
for(int i=0;i<checks.size();i++)
{
 WebElement r = checks.get(i);
 r.click();
}
WebElement uncheck =driver.findElement(By.id("check1"));
uncheck.click();
}
}
