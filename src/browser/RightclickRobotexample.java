package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.interactions.Actions;
import java.util.List;
import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
public class RightclickRobotexample 
{
public static void main(String[] args) throws AWTException 
{
	WebDriver driver = new FirefoxDriver();
	driver.get("https://www.snapdeal.com/");
	driver.manage().window().maximize();
	WebElement we = driver.findElement(By.xpath(".//*[@id='sdHeader']/div[4]/section/div/span[2]/span[4]/a"));
	Actions act = new Actions(driver);
	act.contextClick(we).build().perform();
	Robot rc = new Robot();
	rc.keyPress(KeyEvent.VK_DOWN);
	rc.keyRelease(KeyEvent.VK_DOWN);
	rc.keyPress(KeyEvent.VK_DOWN);
	rc.keyRelease(KeyEvent.VK_DOWN);
	rc.keyPress(KeyEvent.VK_ENTER);
	rc.keyRelease(KeyEvent.VK_ENTER);
}
}
