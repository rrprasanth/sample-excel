package browser;
import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.Point;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import javax.imageio.ImageIO;
import org.openqa.selenium.firefox.FirefoxDriver;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.TakesScreenshot;

public class CroppedScreenshot3 
{
public static void main(String[] args) throws IOException 
{
	WebDriver driver = new FirefoxDriver();
	//driver.get("https://www.cognizant.com/");
	driver.get("https://www.nlcindia.com/new_website/index.htm");
	driver.manage().window().maximize();
	//WebElement we = driver.findElement(By.xpath("html/body/div[1]/div/div[4]/div[4]/div/div/div[1]/a/img"));
	WebElement we = driver.findElement(By.xpath("html/body/table/tbody/tr/td[2]/p[2]/font[2]"));
	File screenshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE); 
	Point p = we.getLocation();
	Rectangle rec = new Rectangle(we.getSize().width,we.getSize().height);
	BufferedImage bi  = ImageIO.read(screenshot);
	BufferedImage dest = bi.getSubimage(p.x, p.y, rec.width, rec.height);
	ImageIO.write(dest, "png", screenshot);
	Date date = new Date();
	SimpleDateFormat dtfmt = new SimpleDateFormat("dd-mm-yyyy hh-MM-ss");
	FileUtils.copyFile(screenshot, new File("D:\\Screenshots\\image\\image-"+ dtfmt.format(date)+".png"));
	}

}
