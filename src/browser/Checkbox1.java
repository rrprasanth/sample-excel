package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
public class Checkbox1 
{	
public static void main(String[] args) 
{
	WebDriver driver = new FirefoxDriver();
	driver.manage().window().maximize();
	driver.get("http://www.seleniumeasy.com/test/basic-checkbox-demo.html");
	WebElement checkbox =driver.findElement(By.id("isAgeSelected"));
	if(checkbox.isSelected())
		System.out.println("Checkbox is already selected");
	else
	{
		checkbox.click();
		System.out.println("Checkbox is selected now");
	}	
}
}
