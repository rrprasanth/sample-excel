package browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
public class Frames 
{
public static void main(String[] args) 
{
		WebDriver driver= new FirefoxDriver();
		driver.get("https://netbanking.hdfcbank.com/netbanking/");	
		driver.manage().window().maximize();
		//driver.findElement(By.xpath("html/body/form/table[2]/tbody/tr/td[2]/table/tbody/tr[1]/td[1]/table/tbody/tr[3]/td[2]/table/tbody/tr[2]/td[2]/span")).sendKeys("123");
		System.out.println("Page source:"+driver.getPageSource());
		System.out.println("Frame count is: "+driver.findElements(By.tagName("frame")).size());
	    driver.switchTo().frame("login_page");
	    try
	    {
	    	driver.findElement(By.xpath(".//input[@name='fldLoginUserId']")).sendKeys("123456");
	    }
	    catch(Exception e)
	    {
	    	System.out.println(e.getMessage());
	    }
	   driver.switchTo().defaultContent();		
}
}
