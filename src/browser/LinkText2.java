package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
public class LinkText2
{
public static void main (String args[])
{
	WebDriver driver = new FirefoxDriver();
	driver.get("http://newtours.demoaut.com/");
	driver.manage().window().maximize();
	WebElement register = driver.findElement(By.linkText("REGISTER"));
	register.click();
	WebElement contact = driver.findElement(By.partialLinkText("CON"));
	contact.click();
}
}