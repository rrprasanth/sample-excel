package browser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.By;

public class Frames1
{
public static void main(String[] args) 
{
	System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://netbanking.hdfcbank.com/netbanking/");
	driver.manage().window().maximize();
	System.out.println("page source is:"+driver.getPageSource());
	System.out.println("Frame count is :"+driver.findElements(By.tagName("frame")).size());
	driver.switchTo().frame("login_page");
	try
	{
		driver.findElement(By.xpath(".//input[@name='fldLoginUserId']")).sendKeys("prasanth");
	}
	catch(Exception e)
	{
		System.out.print(e.getMessage());
	}
}
}
