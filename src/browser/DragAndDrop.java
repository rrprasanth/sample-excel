package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DragAndDrop 

{
public static void main(String[] args) 
{
	WebDriver driver = new FirefoxDriver();
	driver.get("http://jqueryui.com/resources/demos/droppable/default.html");
	driver.manage().window().maximize();
	WebElement drag=driver.findElement(By.xpath(".//*[@id='draggable']"));
	WebElement drop=driver.findElement(By.xpath(".//*[@id='droppable']"));
	Actions act = new Actions(driver);
	act.dragAndDrop(drag,drop).build().perform();

}
}
