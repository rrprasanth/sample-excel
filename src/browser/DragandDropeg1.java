package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class DragandDropeg1 
{
public static void main(String[] args) 
{
		WebDriver driver = new FirefoxDriver();
		driver.get("http://www.seleniumeasy.com/test/drag-and-drop-demo.html");
		driver.manage().window().maximize();
		WebElement drag=driver.findElement(By.xpath(".//*[@id='todrag']/span[1]"));
		WebElement drop=driver.findElement(By.xpath(".//*[@id='mydropzone']"));
		Actions act = new Actions(driver);
		act.dragAndDrop(drag, drop).build().perform();
	}

}
