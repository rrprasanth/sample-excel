package browser;

import static org.testng.AssertJUnit.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class ExplicitWait1 
{
	@Test
	public void testExplicitWait() {
	WebDriver driver = new FirefoxDriver();
	// Launch the sample Ajax application
	driver.get("http://cookbook.seleniumacademy.com/AjaxDemo.html");
	try
	{
	driver.findElement(By.linkText("Page 4")).click();
	WebElement message = new WebDriverWait(driver, 5)
	.until(new ExpectedCondition<WebElement>() 
	{
	public WebElement apply(WebDriver d) 
	{
	return d.findElement(By.id("page4"));
	}
	});
	assertTrue(message.getText().contains("Nunc nibh tortor"));
	} 
	finally 
	{
	driver.quit();
	}
	}

}
