package browser;
import static org.testng.AssertJUnit.assertTrue;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

public class ExplicitWait 
{
	@Test
	public void testExplicitWaitTitleContains()
	{
	//Go to the Google Home Page
	WebDriver driver = new FirefoxDriver();
	driver.get("http://www.google.com");
	//Enter a term to search and submit
	WebElement query = driver.findElement(By.name("q"));
	query.sendKeys("selenium");
	query.click();
	//Create Wait using WebDriverWait.
	/*This will wait for 10 seconds for timeout before title is
	updated with search term*/
	/*If title is updated in specified time limit test will move to
	the text step*/
	//instead of waiting for 10 seconds
	WebDriverWait wait = new WebDriverWait(driver, 10);
	wait.until(ExpectedConditions.titleContains("selenium"));
	//Verify Title
	assertTrue(driver.getTitle().toLowerCase().startsWith("selenium"));
	driver.quit();
}
}
