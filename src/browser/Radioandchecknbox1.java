package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
public class Radioandchecknbox1 
{
public static void main(String[] args) 
{
	WebDriver driver = new FirefoxDriver();
	driver.manage().window().maximize();
	driver.get("http://spicejet.com/");
	WebElement radios = driver.findElement(By.id("ctl00_mainContent_rbtnl_Trip_0"));
	radios.click();
	System.out.println("Radio button Round trip is selected");
	WebElement checkbox = driver.findElement(By.id("ctl00_mainContent_chk_Unmr"));
	checkbox.click();
	System.out.println("check box Unaccompanied Minor is selected");
}
}
