package browser;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;


public class Firefox
{
public static void main (String args[])
{
	WebDriver fx= new FirefoxDriver();
	fx.get("https://www.google.com");
	fx.manage().window().maximize();
	System.out.println("Browser opened");
	fx.close();
	fx.quit();
	
}
}