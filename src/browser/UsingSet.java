package browser;
import java.util.Iterator;
import java.util.Set;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
public class UsingSet 
{
public static void main(String[] args)
{
	System.setProperty("webdriver.chrome.driver", "D://chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("https://accounts.google.com/SignUp");
	driver.manage().window().maximize();
	WebElement link_learnmore=driver.findElement(By.xpath(".//*[@id='initialView']/footer/ul/li[1]/a"));  
	link_learnmore.click();
	Set<String> wins = driver.getWindowHandles();
	System.out.println(wins.size());
	Iterator<String> it =wins.iterator();
	System.out.println(it);
	/*String parentid=wins.get(0);
	String childid=wins.get(1);
	System.out.println("Parent Window Title is - "+driver.getTitle());
	driver.switchTo().window(childid);
	System.out.println("Child Window Title is - "+driver.getTitle());
	driver.findElement(By.xpath(".//*[@id='search-form']/div/input")).sendKeys("hello");
	driver.switchTo().window(parentid);
	System.out.println("Parent Window Title is - "+driver.getTitle());*/	
}
}
