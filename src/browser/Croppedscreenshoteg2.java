package browser;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.Date;
import java.text.SimpleDateFormat;
import org.openqa.selenium.Point;
import org.openqa.selenium.OutputType;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.TakesScreenshot;
import javax.imageio.ImageIO;
import java.io.IOException;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import java.io.File;

public class Croppedscreenshoteg2 
{
public static void main(String[] args) throws IOException 
{
	System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	driver.get("http://www.tatapowersolar.com/");
	driver.manage().window().maximize();
	WebElement we = driver.findElement(By.xpath(".//*[@id='divLogo']/a/img"));
	File screenshot =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	Rectangle rec = new Rectangle(we.getSize().height,we.getSize().width);
	Point loc =we.getLocation();
	BufferedImage bi =ImageIO.read(screenshot);
	BufferedImage destination=bi.getSubimage(loc.x, loc.y, rec.height, rec.width);
	ImageIO.write(destination, "png", screenshot);
	Date date = new Date();
	SimpleDateFormat dateformat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
	FileUtils.copyFile(screenshot,new File( "D:\\Screenshots\\image\\image-"+dateformat.format(date) + ".png"));

}
}
