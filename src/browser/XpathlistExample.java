package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import java.util.List;
public class XpathlistExample 
{
public static void main(String[] args) 
{
	WebDriver driver = new FirefoxDriver();
	driver.manage().window().maximize();
	driver.get("http://smart.gmrgroup.in/gmresourcing/SupplierRegistrationForm.aspx");
	driver.findElement(By.xpath(".//*[@id='rdropPTerms_Arrow']")).click();
	WebElement table=driver.findElement(By.className("rcbList"));
	List<WebElement> down = table.findElements(By.tagName("li"));
	System.out.println(down.size());
	for(WebElement w : down)
	{
		if(w.getText().contains("Irrevoc"))
		{
			w.click();
			break;
		}
	}
}
}
