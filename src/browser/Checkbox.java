package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Checkbox 
{
public static void main(String[] args) 
{
	WebDriver check = new FirefoxDriver();
	check.manage().window().maximize();
	check.get("https://www.nngroup.com/articles/checkboxes-vs-radio-buttons/");
	WebElement box = check.findElement(By.name("permission"));
	WebElement box1=check.findElement(By.name("discardinfo"));
	if(box.isSelected())
			System.out.println("Already box is clicked");
	else
	{
	box.click();
	System.out.println("box is clicked just now");
	}
	if(box1.isSelected())
		System.out.println("box 1 already clicked");
	else
	{
		box1.click();
		System.out.println("box 1 clicked just now");
	}
}
}

