package browser;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.OutputType;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
public class ScreenShotNormal 
{
public static void main(String[] args) throws IOException 
{
	System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
	WebDriver driver = new ChromeDriver();
	//WebDriver driver = new FirefoxDriver();
	driver.get("https://www.facebook.com/");	
	driver.manage().window().maximize();
	File srnsht =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	Date date = new Date();
	SimpleDateFormat dtfmt = new SimpleDateFormat("dd-MM-yy hh-mm-ss");
	FileUtils.copyFile(srnsht,new File("D:\\Screenshots\\image\\image"+dtfmt.format(date)+ ".jpg"));
}
}
