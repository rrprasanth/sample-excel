package browser;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import org.openqa.selenium.OutputType;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
public class ScreenShotNormaleg1 
{
public static void main(String[] args)throws IOException 
{
	WebDriver driver = new FirefoxDriver();
	driver.get("https://twitter.com/");	
	driver.manage().window().maximize();
	File scrnshot = ((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	Date date = new Date();
	SimpleDateFormat dtfmt = new SimpleDateFormat("dd-mm-yyyy hh-MM-ss");
	FileUtils.copyFile(scrnshot,new File("E:\\Screenshots\\image\\image-"+dtfmt.format(date)+".jpg"));
	
}
}
