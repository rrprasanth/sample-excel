package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.By;
public class Radioandcheckbox 
{	
	public static void main(String[] args) 
	{
		WebDriver driver = new FirefoxDriver();
		driver.manage().window().maximize();
		driver.get("https://www.goibibo.com/flights/");
		//WebElement radios = driver.findElement(By.id("gi_roundtrip_label"));
		WebElement radios = driver.findElement(By.xpath(".//*[@id='gi_roundtrip_label']"));
		radios.click();
		System.out.println("round trip is clicked");
		WebElement radios1 = driver.findElement(By.id("student_fare_check"));
		radios1.click();
		System.out.println("student fare selected");
	}
}
