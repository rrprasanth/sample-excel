package browser;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class ReadFromExcel
{
public static void main(String args[]) throws IOException
{
		File f = new File("E:\\Excel\\student.xlsx");
		FileInputStream fis = new FileInputStream(f);
		XSSFWorkbook wb=new XSSFWorkbook(fis);
		//XSSFSheet ws=wb.getSheet("Sheet2");
		XSSFSheet ws=wb.getSheet("Sheet1");
		int rows=ws.getLastRowNum();
		System.out.println(rows);
		int cols = ws.getRow(0).getLastCellNum();
		System.out.println(cols);
		for(int i=0;i<=rows;i++)
		{
				
				String data=ws.getRow(i).getCell(0).getStringCellValue();
				double marks=ws.getRow(i).getCell(1).getNumericCellValue();
				System.out.println(data);
				System.out.println(marks);		
		}
}
}