package browser;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.imageio.ImageIO;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.Point;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class CroppedScreenshoteg1 
{
public static void main(String[] args) throws IOException
{
	//System.setProperty("webdriver.chrome.driver","D:\\chromedriver.exe");
	//WebDriver driver = new ChromeDriver();
	WebDriver driver = new FirefoxDriver();
	//driver.get("http://newtours.demoaut.com/");
	driver.get("https://www.google.com/gmail/about/");
	driver.manage().window().maximize();
	//WebElement we = driver.findElement(By.xpath("html/body/div[1]/table/tbody/tr/td[2]/table/tbody/tr[4]/td/table/tbody/tr/td[2]/table/tbody/tr[2]/td[1]/p[1]/img"));
	WebElement we = driver.findElement(By.xpath("html/body/main/section/div[2]/div[2]/h1"));
	File scnsht =((TakesScreenshot)driver).getScreenshotAs(OutputType.FILE);
	Rectangle rec = new Rectangle(we.getSize().width,we.getSize().height);
	Point loc = we.getLocation();
	BufferedImage bi = ImageIO.read(scnsht);
	BufferedImage destimg=bi.getSubimage(loc.x, loc.y, rec.width, rec.height);
	ImageIO.write(destimg, "png", scnsht);
	Date date = new Date();
	SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy HH-mm-ss");
	FileUtils.copyFile(scnsht, new File("E:\\Screenshots\\image\\image-"+dateFormat.format(date)+".jpg"));
}
}
