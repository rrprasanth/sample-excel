package browser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Chrome
{
public static void main (String args[])
{
	System.setProperty("webdriver.chrome.driver", "E:\\seleniumTool\\driver\\chromedriver.exe");
	WebDriver cd= new ChromeDriver();
	cd.get("https://www.google.com");
	cd.manage().window().maximize();
	cd.findElement(By.xpath("//*[@id=\"tsf\"]/div[2]/div/div[1]/div/div[1]/input")).sendKeys("selenium");
	cd.close();
	cd.quit();
}
}
