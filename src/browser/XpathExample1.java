package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import java.util.List;
public class XpathExample1 
{
public static void main(String[] args) 
{
	WebDriver driver = new FirefoxDriver();
	driver.get("http://toolsqa.com/automation-practice-form/?firstname=&lastname=&photo=&continents=Asia&selenium_commands=Navigation+Commands&selenium_commands=Switch+Commands&selenium_commands=Wait+Commands&submit=");
	driver.manage().window().maximize();
	WebElement we = driver.findElement(By.xpath(".//*[@id='continents']"));
	Select s=new Select(we);
	s.selectByIndex(2);
}
}
