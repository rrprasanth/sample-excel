package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import java.util.List;
public class Multiselectxpath
{
public static void main(String[] args) 
{
	WebDriver driver = new FirefoxDriver();
	driver.manage().window().maximize();
	driver.get("http://www.seleniumeasy.com/test/basic-select-dropdown-demo.html");
	WebElement we=driver.findElement(By.xpath(".//*[@id='multi-select']"));
	Select s = new Select(we);
	s.selectByValue("California");
	s.selectByValue("New Jersey");
	s.selectByValue("New York");
	WebElement we1=driver.findElement(By.xpath(".//*[@id='printAll']"));
	we1.click();
	List<WebElement> options=s.getAllSelectedOptions();
	for(WebElement o : options)
	{
		System.out.println(o.getAttribute("value"));
	}
}
}
