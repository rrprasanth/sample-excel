package browser;
import java.util.ArrayList;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
public class Windows 
{
public static void main(String[] args) throws Exception
{
		System.setProperty("webdriver.chrome.driver", "D:\\chromedriver.exe");
		WebDriver driver= new ChromeDriver();
		//WebDriver driver=new FirefoxDriver();
		driver.get("https://accounts.google.com/SignUp");
		driver.manage().window().maximize();
		//Thread.sleep(5000);
		WebElement link_learnmore=driver.findElement(By.xpath(".//a[text()='Learn more']"));  //Learn more link
		link_learnmore.click();
		ArrayList<String> wins = new ArrayList<String> (driver.getWindowHandles());
		System.out.println(wins.size());
		String parentid=wins.get(0);
		String childid=wins.get(1);
		System.out.println("Parent Window Title is - "+driver.getTitle());
		driver.switchTo().window(childid);
		System.out.println("Child Window Title is - "+driver.getTitle());
		driver.findElement(By.xpath(".//*[@id='search-form']/div/input")).sendKeys("hello");
		driver.switchTo().window(parentid);
		System.out.println("Parent Window Title is - "+driver.getTitle());
}
}
