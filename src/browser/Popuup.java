package browser;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
public class Popuup 
{
public static void main(String[] args) throws InterruptedException 
{
	WebDriver driver = new FirefoxDriver();
	driver.get("http://test1.absofttrainings.com/javascript-alert-confirm-prompt-boxes/");
	driver.manage().window().maximize();
	//driver.findElement(By.xpath(".//button[@id='alert']")).click();
	//driver.findElement(By.xpath(".//*[@id='confirm']")).click();
	driver.findElement(By.xpath(".//*[@id='prompt']")).click();
	Thread.sleep(1000);
	//driver.switchTo().alert().accept();
	//driver.switchTo().alert().dismiss();
	driver.switchTo().alert().sendKeys("Prasanth");
	driver.switchTo().alert().accept();
}
}
